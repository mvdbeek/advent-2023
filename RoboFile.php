<?php

use AdventOfCode\DayFive\AlmanacLineIterator;
use AdventOfCode\DayFour\CardCalculator;
use AdventOfCode\DayFour\ScoreCalculator;
use AdventOfCode\DayOne\TrebuchetCalibrator;
use AdventOfCode\DayTwo\GameRecord;
use Robo\Symfony\ConsoleIO;

require_once 'vendor/autoload.php';

/**
 * This is project's console commands configuration for Robo task runner.
 *
 * @see https://robo.li/
 */
class RoboFile extends \Robo\Tasks
{
    public function day_one_part_one(ConsoleIO $io): void
    {
        $calculator = new TrebuchetCalibrator();

        $io->say("Solution: " . $calculator->calculateCalibrationProduct('src/DayOne/input/input_puzzle_1.txt'));
    }
    public function day_one_part_two(ConsoleIO $io): void
    {
        $calculator = new TrebuchetCalibrator();

        $io->say("Solution: " . $calculator->calculateCalibrationProductWithNumbers('src/DayOne/input/input_puzzle_1.txt'));
    }

    public function day_two_part_one(ConsoleIO $io): void
    {
        $record = GameRecord::extractFromString("Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green");
    }

    public function day_four_part_one(ConsoleIO $io): void
    {
        $calculator = new ScoreCalculator();

        $io->say("Solution: " . $calculator->calculateFromFile('src/DayFour/input/input_puzzle_1.txt'));
    }

    public function day_four_part_two(ConsoleIO $io): void
    {
        $calculator = new CardCalculator();

        $io->say("Solution: " . $calculator->calculateFromFile('src/DayFour/input/input_puzzle_1.txt'));
    }

    public function day_five_part_one(ConsoleIO $io): void
    {
        $almanac = AlmanacLineIterator::fromFile('src/DayFive/input/input_puzzle_1.txt');

        $io->say("Solution: " . $almanac->getNearestLocation());
    }

    public function day_five_part_two(ConsoleIO $io): void
    {
        $almanac = \AdventOfCode\DayFive\AlmanacLineIterator::fromFile('src/DayFive/input/input_puzzle_1.txt');

        $io->say("Solution: " . $almanac->getNearestLocationForSeedRanges());
    }
}
