<?php

namespace AdventOfCode\DayFive;

class Almanac
{
    public function __construct(public array $seeds, public array $seedRanges, public array $maps)
    {
    }

    public function getNearestLocation(): int
    {
        $locations = [];
        foreach ($this->seeds as $seed) {
            $locations[$seed] = $this->getDestination($seed);
        }

        return min($locations);
    }

    public function getNearestLocationForSeedRanges(): int
    {
        $location = 0;

        foreach ($this->seedRanges as $seedRange) {
            $seed = $seedRange[0];
            $max = $seed + $seedRange[1];
            while ($seed <= $max) {
                $newLocation = $this->getDestination($seed);
                if ($location > $newLocation || $location === 0) {
                    $location = $newLocation;
                }
                $seed++;
            }
        };

        return $location;
    }

    public function getNextMap(string $from): ?Map
    {
        $maps = array_filter($this->maps, function (Map $map) use ($from) {
            return $map->from === $from;
        });
        if ($maps === []) {
            return null;
        }

        if (count($maps) !== 1) {
            throw new \RuntimeException('Should be only one map');
        }

        return reset($maps);
    }

    public function getDestination(int $seed): int
    {
        $destinationValue = (int)$seed;
        $from = 'seed';
        while ($from !== null) {
            $map = $this->getNextMap($from);
            if (!$map) {
                $from = null;
                continue;
            }
            $destinationValue = $map->getDestination($destinationValue);
            $from = $map->to;
        }
        return $destinationValue;
    }
}
