<?php

namespace AdventOfCode\DayFive;

class AlmanacLineIterator
{
    public static function fromFile(string $filename): Almanac
    {
        $file = file($filename);
        $seeds = [];
        $map = [];
        $mapCollection = [];
        $seedsRange = [];
        foreach ($file as $line) {
            $line = trim($line);

            if (str_starts_with($line, "seeds:")) {
                $seeds = self::getSeeds($line);
                $seedsRange = self::getSeedRanges($line);
                continue;
            }

            if ($line === "" && count($map)) {
                $mapCollection[] = self::getMapFromArray($map);
                $map = [];
                continue;
            }

            if ($line === "") {
                continue;
            }

            $map[] = $line;
        }

        $mapCollection[] = self::getMapFromArray($map);

        return new Almanac($seeds, $seedsRange, $mapCollection);
    }

    private static function getSeeds(string $line): array
    {
        $seedsString = str_replace('seeds: ', '', $line);

        return explode(' ', $seedsString);
    }

    private static function getSeedRanges(string $line): array
    {
        $seeds = self::getSeeds($line);
        return array_chunk($seeds, 2);
    }

    private static function getMapFromArray(array $lines): Map
    {
        $ranges = [];
        foreach ($lines as $line) {
            if (str_contains($line, "map:")) {
                $title = str_replace(' map:', '', $line);
                $fromAndTo = explode('-to-', $title);
                continue;
            }

            $range = explode(" ", $line);
            $ranges[] = ['destination' => (int) $range[0], 'source' => (int) $range[1], 'range' => (int) $range[2]];
        }

        return new Map($fromAndTo[0], $fromAndTo[1], $ranges);
    }
}
