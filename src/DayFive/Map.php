<?php

namespace AdventOfCode\DayFive;

class Map
{
    public function __construct(public string $from, public $to, public array $ranges)
    {
    }

    public function getDestination(int $source): int
    {
        foreach ($this->ranges as $range) {

            if ($source >= $range['source'] && $source < $range['source'] + $range['range']) {
                return $range['destination'] + ($source - $range['source']);
            }
        }

        return $source;
    }
}
