<?php

namespace AdventOfCode\DayFour;

class CardCalculator
{
    public function calculateFromFile(string $file): int
    {
        $originalCards = file($file);
        $cards = [];
        $totalcards = 0;

        foreach ($originalCards as $card) {
            $totalcards++;

            $scratchcard = ScratchCard::createFromString($card);

            $originalCardNumber = $scratchcard->cardNumber;
            $matches = $scratchcard->matches();

            if (!array_key_exists($scratchcard->cardNumber, $cards)) {
                $cards[$scratchcard->cardNumber] = ['copies' => 0];
            }

            $totalcards += $cards[$originalCardNumber]['copies'];
            if(!$matches) {
                continue;
            }

            $copyIterator = $cards[$originalCardNumber]['copies'] + 1;

            while ($copyIterator) {
                $copyIterator--;
                $matchIterator = $matches;
                while ($matchIterator) {
                    $cardNumber = $matchIterator + $originalCardNumber;

                    if (!array_key_exists($cardNumber, $cards)) {
                        $cards[$cardNumber] = ['copies' => 0];
                    }

                    $matchIterator--;
                    $cards[$cardNumber]['copies']++;
                }
            }
        }
        return $totalcards;
    }
}
