<?php

namespace AdventOfCode\DayFour;

class ScoreCalculator
{
    public function calculateFromFile(string $file): int
    {
        $cards = file($file);
        $score = 0;

        foreach ($cards as $card) {
            $scratchcard = ScratchCard::createFromString($card);
            $score += $scratchcard->score();
        }

        return $score;
    }
}
