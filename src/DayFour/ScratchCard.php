<?php

namespace AdventOfCode\DayFour;

class ScratchCard
{
    /**
     * @param int[] $winningNumbers
     * @param int[] $numbers
     */
    private function __construct(public int $cardNumber, public array $winningNumbers, public array $numbers)
    {
    }

    public static function createFromString(string $string): self
    {
        $nameAndNumbers = explode(":", $string);
        $cardNumber = (int) str_replace("Card", "", $nameAndNumbers[0]);
        $winningAndNumbers = explode("|", $nameAndNumbers[1]);
        $winningNumbers =  explode(" ", trim($winningAndNumbers[0]));
        $numbers = array_values(array_filter(explode(" ", trim($winningAndNumbers[1]))));
        return new self($cardNumber, $winningNumbers, $numbers);
    }

    public function score(): int
    {
        $score = 0;
        foreach ($this->numbers as $number) {
            if (!in_array($number, $this->winningNumbers)) {
                continue;
            }
            if ($score === 0) {
                $score = 1;
                continue;
            }

            $score *= 2;
        }

        return $score;
    }

    public function matches(): int
    {
        $matches = 0;
        foreach ($this->numbers as $number) {
            if (!in_array($number, $this->winningNumbers)) {
                continue;
            }
            $matches++;
        }
        return $matches;
    }
}
