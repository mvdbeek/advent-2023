<?php

namespace AdventOfCode\DayOne;

class TrebuchetCalibrator
{
    public const STRING_NUMBERS = '/(one)|(two)|(three)|(four)|(five)|(six)|(seven)|(eight)|(nine)/';
    public const INT_NUMBERS = '${1:+1}';

    public function calculateCalibrationProduct(string $inputFile): int
    {
        $input = file($inputFile);
        $product = 0;
        foreach ($input as $line) {
            $product += $this->parseNumbers($line);
        }
        return $product;
    }

    public function calculateCalibrationProductWithNumbers(string $inputFile): int
    {
        $input = file($inputFile);
        $product = 0;
        foreach ($input as $line) {
            $product += $this->parseNumbersWithWords($line);
        }
        return $product;
    }

    private function parseNumbers(string $line): int
    {
        $start_pattern = "/(\d)/";
        $end_pattern = "/.*(\d)/";

        $startMatches = [];
        preg_match($start_pattern, $line, $startMatches);
        $first = $this->convertStringToInt($startMatches[1] ?? "");
        $endMatches = [];
        preg_match($end_pattern, $line, $endMatches);
        $last = $this->convertStringToInt($endMatches[1] ?? "");
        return (int)"$first$last";
    }

    private function parseNumbersWithWords(string $line): int
    {
        $start_pattern = "/(\d|one|two|three|four|five|six|seven|eight|nine)/";
        $end_pattern = "/.*(\d|one|two|three|four|five|six|seven|eight|nine)/";

        $startMatches = [];
        preg_match($start_pattern, $line, $startMatches);
        $first = $this->convertStringToInt($startMatches[1] ?? "");
        $endMatches = [];
        preg_match($end_pattern, $line, $endMatches);
        $last = $this->convertStringToInt($endMatches[1] ?? "");
        return (int)"$first$last";
    }

    private function convertStringToInt(string $word): int
    {
        if ((int)$word) {
            return (int)$word;
        }

        if ($word == "one") {
            return 1;
        }

        if ($word == "two") {
            return 2;
        }

        if ($word == "three") {
            return 3;
        }

        if ($word == "four") {
            return 4;
        }

        if ($word == "five") {
            return 5;
        }

        if ($word == "six") {
            return 6;
        }

        if ($word == "seven") {
            return 7;
        }

        if ($word == "eight") {
            return 8;
        }

        if ($word == "nine") {
            return 9;
        }

        throw new \RuntimeException('No match!');
    }
}
