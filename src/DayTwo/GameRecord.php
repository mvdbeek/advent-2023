<?php

namespace AdventOfCode\DayTwo;

class GameRecord
{
    private function __construct(public int $id, public array $rounds)
    {

    }

    public static function extractFromString(string $string): self
    {
        $idAndRounds = explode(":", $string);
        $id = (int)str_replace("Game ", "", $idAndRounds[0]);
        $rounds = explode(";", $idAndRounds[1]);
        $extractedRounds = [];
        foreach ($rounds as $index => $round) {
            $stones = explode(',', $round);

            $extractedRounds['blue'] = 0;
            $extractedRounds['green'] = 0;
            $extractedRounds['red'] = 0;

            foreach ($stones as $stone) {
                $stoneAndColor = explode(" ", trim($stone));
                $extractedRounds[$index][$stoneAndColor[1]] = (int) $stoneAndColor[0];
            }
        }

        return new self($id, $extractedRounds);
    }

    public function validWith(int $blue, int $green, int $red): bool
    {
        foreach ($this->rounds as $round) {
            if ($round['blue']   > $blue) {
                var_dump($round, $blue);
                return false;
            }
        }
        return true;

    }
}
