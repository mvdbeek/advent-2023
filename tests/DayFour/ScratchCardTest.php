<?php

namespace DayFour;

use AdventOfCode\DayFour\ScratchCard;
use PHPUnit\Framework\TestCase;

class ScratchCardTest extends TestCase
{
    public function test_creates_scratch_scard(): void
    {
        // Arrange
        $string = 'Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53';
        // Act
        $card = ScratchCard::createFromString($string);
        // Assert
        self::assertSame(['41', '48', '83', '86', '17'], $card->winningNumbers);
        self::assertSame(['83', '86', '6', '31', '17', '9', '48', '53'], $card->numbers);
    }

    public function test_score(): void
    {
        // Arrange
        $string = 'Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53';
        $card = ScratchCard::createFromString($string);

        // Act
        $result = $card->score();

        // Assert
        self::assertSame(8, $result);
    }

    public function test_card_id(): void
    {
        // Arrange
        $string = 'Card  71: 41 48 83 86 17 | 83 86  6 31 17  9 48 53';
        $card = ScratchCard::createFromString($string);

        // Act
        $result = $card->cardNumber;

        // Assert
        self::assertSame(71, $result);
    }

    public function test_matches(): void
    {
        $string = 'Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53';
        $card = ScratchCard::createFromString($string);

        // Act
        $result = $card->matches();

        // Assert
        self::assertSame(4, $result);
    }
}
